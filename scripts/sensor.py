# The BME680 includes sensors for temperature, humidity, pressure and gas content

import sys
import time
import smbus
import os
import json

from bme680 import BME680
from http.server import HTTPServer, BaseHTTPRequestHandler

class balenaSense():
    readfrom = 'unset'
    bus = smbus.SMBus(1)

    def __init__(self):
        # Next, check to see if there is a BME680 on the I2C bus
        if self.readfrom == 'unset':
            try:
                self.bus.write_byte(0x76, 0)
            except IOError:
                print('BME680 not found on 0x76, trying 0x77')
            else:
                print('BME680 found on 0x76')
                self.readfrom = 'bme680primary'
                self.sensor = BME680(self.readfrom)

        # If we didn't find it on 0x76, look on 0x77
        if self.readfrom == 'unset':
            try:
                self.bus.write_byte(0x77, 0)
            except IOError:
                print('BME680 not found on 0x77')
            else:
                print('BME680 found on 0x77')
                self.readfrom = 'bme680secondary'
                self.sensor = BME680(self.readfrom)

        if self.readfrom == 'unset':
            print('No suitable sensors found! Exiting.')
            sys.exit()

    def sample(self):
        return self.apply_offsets(self.sensor.get_readings(self.sensor))


    def apply_offsets(self, measurements):
        # Apply any offsets to the measurements before storing them in the database
        if os.environ.get('BALENASENSE_TEMP_OFFSET') != None:
            measurements[0]['fields']['temperature'] = measurements[0]['fields']['temperature'] + float(os.environ['BALENASENSE_TEMP_OFFSET'])

        if os.environ.get('BALENASENSE_HUM_OFFSET') != None:
            measurements[0]['fields']['humidity'] = measurements[0]['fields']['humidity'] + float(os.environ['BALENASENSE_HUM_OFFSET'])

        if os.environ.get('BALENASENSE_ALTITUDE') != None:
            # if there's an altitude set (in meters), then apply a barometric pressure offset
            altitude = float(os.environ['BALENASENSE_ALTITUDE'])
            measurements[0]['fields']['pressure'] = measurements[0]['fields']['pressure'] * (1-((0.0065 * altitude) / (measurements[0]['fields']['temperature'] + (0.0065 * altitude) + 273.15))) ** -5.257

        return measurements



class balenaSenseHTTP(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        measurements = balenasense.sample()
        self.wfile.write(json.dumps(measurements[0]['fields']).encode('UTF-8'))

    def do_HEAD(self):
        self._set_headers()


# Start the server to answer requests for readings
balenasense = balenaSense()

while True:
    server_address = ('', 8000)
    httpd = HTTPServer(server_address, balenaSenseHTTP)
    print('Sensor HTTP server running')
    httpd.serve_forever()
